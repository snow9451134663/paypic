<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\model\PayPicModel;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\ErrorCode;

class PayPicController extends ApiController
{
    use ApiTrait\CommonFunctions;
    //
    public function index()
    {
        echo 333;die;
    }

    public function addPic(request $request){

        $this->writeLog('addpic', 'post data', $request->all());

        $bRes = $this->validatorInput($request->all(), PaypicModel::$uploadpicValidator);

        if (!$bRes) {
        
            return $this->outputJson($this->code, $this->data, $this->msg);
        }

        $sPath = $this->uploadPicByApi($request->get('pic'), $request->get('code_id'));
        
        if(!$sPath) {

            return $this->outputJson($this->code, $this->data, $this->msg);
        }
        
        $oModel = App(PayPicModel::class);
        $oModel->fill(['name'=>$request->get('name')]);
        $oModel->fill(['phone'=>encrypt($request->get('phone'))]);
        $oModel->fill(['code_id'=>$request->get('code_id')]);
        $oModel->fill(['pic'=>$sPath]);
        $oModel->fill(['platform'=>$request->get('platform')]);
        $oModel->save();

        return $this->outputJson(ErrorCode::CODE_SUCCESS, '', ErrorCode::$display[ErrorCode::CODE_SUCCESS]);

    }
}
