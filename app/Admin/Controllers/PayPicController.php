<?php

namespace App\Admin\Controllers;

use App\model\PayPicModel;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PayPicController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '充值列表';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PayPicModel);

        $grid->column('id', __('Id'))->sortable();
        $grid->column('name', __('姓名'));
        $grid->column('phone', __('电话'))->display(function($phone) {
            return decrypt($phone);
        });
        $grid->column('pic', __('截图'))->display(function($pic) {
            return '<a href='.env('APP_URL').'/storage/'.$pic.' target=_blank>'.$pic.'</a>';
        });
        $grid->column('code_id', __('订单号'));
        $grid->column('platform', __('平台'));
        $grid->column('created_at', __('生成时间'));
        $grid->column('updated_at', __('修改时间'));
        $grid->paginate(100);
        $grid->disableCreateButton();
        $grid->disableActions();
        $grid->model()->orderBy('id', 'desc');

        $grid->filter(function($filter){

            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            // 在这里添加字段过滤器
            $filter->like('name', '姓名');

            $filter->where(function ($query) {
                $query->where('phone', 'like', encrypt("%{$this->input}%"));
            }, '电话');
            
            $filter->like('code_id', '订单号');
            
            $filter->between('created_at', '生成时间')->datetime();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PayPicModel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('phone', __('Phone'));
        $show->field('pic', __('Pic'));
        $show->field('code_id', __('Code id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PayPicModel);

        $form->text('name', __('Name'));
        $form->number('phone', __('Phone'));
        $form->image('pic', __('Pic'));
        $form->number('code_id', __('Code id'));

        return $form;
    }
}
