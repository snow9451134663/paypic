<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class PayPicModel extends Model
{
    protected $table = 'paypic';
    
    public $timestamps = true;

    protected $fillable = ['name', 'phone', 'code_id', 'pic', 'platform'];

    public static $uploadpicValidator = [
        'name'=>'required',
        'phone'=>'required',
        'pic'=>'required',
        'code_id'=>'required',
    ];

    /**
     * 新增数据
     */
    public function addData(Array $aData)
    {
        $this->name = $aData['name'];

        $this->phone = $aData['phone'];

        $this->code_id = $aData['code_id'];

        $path = $this->uploadPic($aData['pic']);
    }

}
