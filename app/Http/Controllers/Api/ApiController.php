<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public $code = null;
    public $data = null;
    public $msg  = null;

    public function outputJson($code = 200, $data = null, $msg = '')
    {
        return response()->json(['code' => $code, 'data' => $data, 'msg'=>$msg]);
    }
}
