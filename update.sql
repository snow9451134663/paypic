/****************************2019年12月4日14:24:44**************************/
CREATE TABLE `paypic` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '姓名',
  `phone` varchar(255) NOT NULL COMMENT '手机号码',
  `pic` varchar(100) NOT NULL COMMENT '图片地址',
  `code_id` varchar(30) NOT NULL COMMENT '订单号',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4