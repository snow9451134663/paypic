<?php
namespace App\Http\Controllers\Api\ApiTrait;

use App\Http\Controllers\ErrorCode;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

//公共方法定义trait
trait CommonFunctions
{
    /**
     * [validatorInput 表单验证]
     * @param
     * @author Snow
     * @date   2018-08-07T18:04:05+0800
     * @param  Request                  $request [description]
     * @return [type]                            [description]
     */
    public function validatorInput($aData, $aRules)
    {

        $validator = Validator::make($aData, $aRules, ErrorCode::$validatorMsg);
        
        if ($validator->fails()) {

            $error = $validator->errors()->all()[0];
            
            $this->code = 299;
            
            $this->data = null;
            
            $this->msg = $error;
            
            return false;
        }
        
        return true;
    }

    public function uploadPicByApi($img, $sName)
    {

        preg_match('/^(data:\s*image\/(\w+);base64,)/', $img, $aResult);

        if(!$aResult) {

            $this->code = 203;
            
            $this->data = null;
            
            $this->msg = ErrorCode::$display[$this->code];
            
            return false;
        }

        if(!in_array($aResult[2], ['jpg','jpeg','png', 'gif', 'bmp'])) {

            $this->code = 204;
            
            $this->data = null;
            
            $this->msg = ErrorCode::$display[$this->code];
            
            return false;
        }

        $aImage = explode(',', $img);

        // $fileName = date('Y_m_d').'/' . $sName .'.'. $aResult[2];
        $fileName = date('Y_m_d').'/' . time().rand(1000, 9999) .'.'. $aResult[2];

        $oFile = Storage::disk('public');

        $bRes = $oFile->put($fileName, base64_decode($aImage[1]));
        
        if(!$bRes) {

            $this->code = 205;
            
            $this->data = null;
            
            $this->msg = ErrorCode::$display[$this->code];
            
            return false;
        }

        return $fileName;

    }
    /**
     * [writeLog 自定义日志方法]
     * @param
     * @author Snow
     * @date   2018-09-17T18:28:15+0800
     * @param  [type]                   $sName    [文件夹名称]
     * @param  [type]                   $sMsg     [日志消息]
     * @param  [type]                   $aContent [日志内容]
     * @param  string                   $sLevel   [日志级别]
     * @return [type]                             [description]
     */
    public function writeLog($sName, $sMsg, $aContent = [], $sLevel = 'info')
    {
        $aLevel = [
            'info'=>Logger::INFO,
            'debug'=>Logger::DEBUG,
            'notice'=>Logger::NOTICE,
            'warning'=>Logger::WARNING,
            'error'=>Logger::ERROR,
            'critical'=>Logger::CRITICAL,
            'alert'=>Logger::ALERT,
            'emergency'=>Logger::EMERGENCY
        ];
        $method = 'add'.ucfirst($sLevel);
        // Create the logger
        $logger = new Logger(ENV('APP_NAME'));
        // Now add some handlers
        $logger->pushHandler(new StreamHandler(
            storage_path('logs/'.$sName.'/'.date('Y-m-d').'-'.$sLevel.'.log'),
            $aLevel[$sLevel]
        ));
        // You can now use your logger
        $logger->$method($sMsg, $aContent);
    }
}