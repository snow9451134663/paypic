<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorCode extends Controller
{
    const CODE_SUCCESS = 200;
    const CODE_ERROR_PIC_TYPE = 203;
    const CODE_ERROR_PIC = 204;
    const CODE_ERROR_STATUS = 205;
    const CODE_ERROR_CAPTCHA = 206;
    const CODE_ERROR_CAPTCHA_EXPIRED = 207;
    const CODE_ERROR_LOGIN_EXPIRED = 255;
    const CODE_ERROR_BUSY = 261;
    const CODE_ERROR_SMS_CODE = 262;
    const CODE_ERROR_SMS_CODE_EXPIRED = 263;
    const CODE_ERROR_NO_DATA = 280;
    const CODE_ERROR_NO_ACTION = 281;

    static public $display = [
        200 => '成功',
        203 => '图片类型错误',
        204 => '图片错误',
        205 => '上传失败',
        206 => '验证码错误',
        207 => '验证码已过期',
        255 => '登陆状态失效，请重新登陆',
        261 => '操作太频繁，请稍后',
        262 => '手机验证码错误',
        263 => '手机验证码已过期',
        280 => 'NO DATA',
        281 => 'NO ACTION',
        299 => '自定义错误',
    ];
    public static $validatorMsg = [
        'name.required' => '用户名不能为空',
        'phone.required'=>'手机号码不能为空',
        'pic.required'=>'图片不能为空',
        'code_id.required'=>'订单号不能为空',
    ];
}
